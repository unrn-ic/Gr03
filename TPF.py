print('Para encender la calculadora, presione el botón "On"')
on = input(' ')
while on != 'On':
    on = input('ERROR: Botón incorrecto, presione el botón "On" para encender la calculadora.\n')

# Logo ASCII para una interfaz de usuario atractiva al ojo
logo_calculadora = ''' 

  .oooooo.             oooo                        oooo                  .o8                               
 d8P'  `Y8b            `888                        `888                 "888                               
888           .oooo.    888   .ooooo.  oooo  oooo   888   .oooo.    .oooo888   .ooooo.  oooo d8b  .oooo.   
888          `P  )88b   888  d88' `"Y8 `888  `888   888  `P  )88b  d88' `888  d88' `88b `888""8P `P  )88b  
888           .oP"888   888  888        888   888   888   .oP"888  888   888  888   888  888      .oP"888  
`88b    ooo  d8(  888   888  888   .o8  888   888   888  d8(  888  888   888  888   888  888     d8(  888  
 `Y8bood8P'  `Y888""8o o888o `Y8bod8P'  `V88V"V8P' o888o `Y888""8o `Y8bod88P" `Y8bod8P' d888b    `Y888""8o                                                                                                                                                                                                                                            
'''


if on == 'On': # Si el usuario ingresa (presiona) "On", se inicia la calculadora
    print(logo_calculadora)
    print('-' * 90)
    print('-' * 90)
    while True:
        print('_' * 51)
        opcion = input('ELIJA EL MODO DE CALCULO CON EL QUE DESEA TRABAJAR:\n''\n1: CALCULADORA CLÁSICA\n2: CALCULADORA DE FRACCIONES\n3: CONVERTIDOR DE NÚMEROS DECIMALES\n4: APAGAR CALCULADORA\n \nOpción:  ')
        
        if opcion == '4': # Opcion 4 para salir de la calculadora
            print('\nApagando calculadora...')
            break

        if opcion == '1': # Opcion 1 - Calculadora Clásica
            print('------------------\nCALCULADORA CLÁSICA\n------------------')
            def numero_valido(): # Se define un número válido para que la calculadora pueda operar
                while True:
                    numero = input("Ingrese número: ")
                    try:
                        numero = float(numero)
                        break;
                    except ValueError:
                        print("ERROR: Ingrese únicamente números válidos.")
                return numero
            def operador_valido(): # Se define un operador válido para que la calculadora realice la operación
                while True:
                    operador = input("Ingrese operador -> (+, -, / , * , =): ")
                
                    while operador not in ["*", "/", "-", "+", "="]: # Si el operador no se encuentra dentro de los permitidos, generará un mensaje de error
                        print("ERROR: El valor ingresado no se encuentra en los parámetros permitidos. Intente nuevamente.")
                        operador = input("Reingrese un operador -> (+, -, / , * , =): ")
                    break
                return operador
            def comparacion(resultado, operador, calculo): # 
                numero_x = numero_valido()
                if operador != "/":
                    calculo += operador + " "
                    if operador == "+":
                        resultado += numero_x
                    elif operador == "-":    
                        resultado -= numero_x
                    elif operador == "*":
                        resultado *= numero_x
                    calculo += str(numero_x) + " "
                else:
                    if numero_x == 0:
                            print("ERROR")
                    else:
                        calculo += operador + " "
                        calculo += str(numero_x) + " "
                        resultado /= numero_x    
                return resultado, calculo
            
            cantidad_numeros = False
            calculo = ""
            
            while True:
                if cantidad_numeros == False:
                    resultado = numero_valido()
                    calculo += str(resultado) + " "
                    cantidad_numeros = True
                else:
                    operador = operador_valido()
                    if operador == "=":
                        calculo += operador + " "
                        print(calculo, resultado)
                        break;
                    else:
                        resultado, calculo = comparacion(resultado, operador, calculo)
    
        
        if opcion == '2': # Si opcion == '4' se accede a Calculadora de fracciones
            print('-------------------------\nCALCULADORA DE FRACCIONES\n-------------------------')
        
            #Ingresar fracciones
            while True:
                try:
                    numerador1 = int(float(input("Ingrese el numerador: ")))
                    denominador1 = int(float(input("Ingrese el denominador: ")))
            
                    if denominador1 == 0:
                        print("Error: El denominador no puede ser cero.")
                        continue
            
                    # Si ambos valores son números, salir del bucle
                    break
                except ValueError:
                    print("Error, el valor ingresado no es un número. Intente nuevamente.")
                
            #seleccionar operador
            operador = []
            cont_fracc = 1
            while operador != "=":
    
                operador = input("Ingrese un operador (*, /, -, +, =): ")

                while operador not in ["*", "/", "-", "+", "="]:
                    print("Error, el valor ingresado no se encuentra en los parámetros permitidos. Intente nuevamente.")
                    operador = input("Reingrese un operador (*, /, -, +, =): ")
    
                if operador != "=":
                    while True:
                        try:
                            numerador2 = int(float(input("Ingrese el siguiente numerador: ")))
                            denominador2 = int(float(input("Ingrese el siguiente denominador: ")))
                            
                            if denominador2 == 0:
                                print("Error, El denominador no puede ser cero.")
                                continue
                    
                            # Si ambos valores son números, salir del bucle
                            break
                        except ValueError:
                            print("Error, el valor ingresado no es un número. Intente nuevamente.")
    
                # Suma
                if operador == "+":
                    # encontrar MCM
                    i = 2 
                    while True:
                        if i % denominador1 == 0 and i % denominador2 == 0:
                                mcm = i
                                break
                        i += 1
                        
                    if cont_fracc == 1:
                        print(f"{numerador1}/{denominador1} + {numerador2}/{denominador2} ")
                    else:
                        print(f"Ans + {numerador2}/{denominador2} ")
                    
                    # operando con fracciones
                    numerador1_suma = (mcm / denominador1) * numerador1
                    
                    numerador2_suma = (mcm / denominador2) * numerador2
                    
                    denominador1 = mcm
                    
                    numerador1 = int(numerador1_suma + numerador2_suma)
                    
                # Resta
                elif operador == "-":
                    # encontrar MCM
                    i = 2 
                    while True:
                        if i % denominador1 == 0 and i % denominador2 == 0:
                                mcm = i
                                break
                        i += 1
                        
                    if cont_fracc == 1:
                        print(f"{numerador1}/{denominador1} - {numerador2}/{denominador2} ")
                    else:
                        print(f"Ans - {numerador2}/{denominador2} ")
                    
                    # operando con fracciones
                    numerador1_suma = (mcm / denominador1) * numerador1
                    
                    numerador2_suma = (mcm / denominador2) * numerador2
                    
                    denominador1 = mcm
                    
                    numerador1 = int(numerador1_suma - numerador2_suma)
            
                # Multiplicación    
                elif operador == "*":
                    if cont_fracc == 1:
                        print(f"{numerador1}/{denominador1} * {numerador2}/{denominador2} ")
                    else:
                        print(f"Ans * {numerador2}/{denominador2} ")
                        
                    numerador1 = numerador1 * numerador2
                    
                    denominador1 = denominador1 * denominador2 
            
                # Division
                elif operador == "/":
                    if cont_fracc == 1:
                        print(f"{numerador1}/{denominador1} : {numerador2}/{denominador2} ")
                    else:
                        print(f"Ans : {numerador2}/{denominador2} ")
                        
                    numerador1 = numerador1 * denominador2 
                    
                    denominador1 = denominador1 * numerador2
                    
                cont_fracc += 1
        
            # imprimir resultado
            print(f"\nAns = {numerador1}/{denominador1}")



        if opcion == '3': # Si opcion es igual a '3', se iniciará el convertidor de numeros decimales
            numero = int(input('\n--------------------------------\nCONVERTIDOR DE NÚMEROS DECIMALES\n--------------------------------\n \nIngrese el número a convertir: ')) # Ingresar número decimal
            while numero < 0:
                print("ERROR: E1l valor a ingresar no puede ser menor a 0. Reingrese el número.")
                numero = int(input(""))
            print('\nSeleccione qué modo quiere utilizar:\nbin: DECIMAL A BINARIO\noct: DECIMAL A OCTAL\nhex: DECIMAL A HEXADECIMAL') # Seleccionar un modo de conversión
            while True:
                modo = input('\nModo: ')

                if modo == 'bin': # Decimal a binario
                    print('-' * 33 + '\n' +
                    'HA SELECCIONADO DECIMAL A BINARIO' + '\n' +
                    '-' * 33)
                    lista_restos_str = ''
                    while numero > 0:
                        resto = numero % 2
                        lista_restos_str += str(resto)  
                        numero //= 2  
                    lista_restos_str = lista_restos_str[::-1]
                    print('\nRESULTADO:' , lista_restos_str)
                    break

                if modo == 'oct': # Decimal a octal
                    print('-' * 31 + '\n' +
                    'HA SELECCIONADO DECIMAL A OCTAL' + '\n' +
                    '-' * 31)
                    lista_restos_str = ''
                    while numero > 0:
                        resto = numero % 8
                        lista_restos_str += str(resto)
                        numero //= 8
                    lista_restos_str = lista_restos_str[::-1]
                    print('\nRESULTADO:' , lista_restos_str)
                    break

                if modo == 'hex': # Decimal a hexadecimal
                    print('-' * 37 + '\n' +
                    'HA SELECCIONADO DECIMAL A HEXADECIMAL' + '\n' +
                    '-' * 37)

                    lista_hex = {10: 'A', 11: 'B', 12: 'C', 13: 'D', 14: 'E', 15: 'F'} # Diccionario con elementos numéricos definidos por letras equivalentes a hexadecimal
                    
                    resultado = ''
                    
                    while numero > 0:
                        resto = numero % 16
                        if resto in lista_hex: # Si el resto se encuentra en el diccionario, se mostrará su letra equivalente, caso contrario se mostrará el resto numérico
                            resultado += lista_hex[resto]
                        else:
                            resultado += str(resto)
                        numero //= 16
                    
                    resultado = resultado[::-1] # Al resultado del anterior bucle se le invierte para que exprese el núm. hexadecimal correctamente
                    
                    print('\nRESULTADO: ', resultado, ' \n')
                    break

                else:
                    print('ERROR: Seleccione un modo de conversión válido.') # Mensaje de error cuando el modo no es válido
                    continue
